﻿//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using MyJSONService.DataAccess.Domian;
using MyJSONService.DataAccess.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace MyJSONService.DataAccess
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static class FourSquareApi
    {
        private static readonly string _clientId = "YBAMYVCWP4MVPOVBFLXANTOJYB4PIWUI3KTB1LB2QMXVMPPO";
        private static readonly string _clientSecret = "S1JGAMNPJECWVFIFPVAMZVN3LSRYLNF2EBEYNA5P3RSA2X2M";

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static List<ReviewContainer> GetReviews(string placeId)
        {
            try
            {
                // Бюро бургеры - "5925c20be97dfb49b51d892b"
                // example place id = "49d51ce3f964a520675c1fe3"

                var getReviewUrl = $"https://api.foursquare.com/v2/venues/{placeId}?v=20131016&client_id={_clientId}&client_secret={_clientSecret}";

                FourSquareRootObject obj = new FourSquareRootObject();

                using (WebClient wc = new WebClient() { Encoding = Encoding.UTF8 })
                {
                    var json = wc.DownloadString(getReviewUrl);


                    using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                    {
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
                        obj = (FourSquareRootObject)serializer.ReadObject(ms);
                    }
                }

                if (obj != null && obj.response != null && obj.response.venue != null && obj.response.venue.tips != null && obj.response.venue.tips.groups != null && obj.response.venue.tips.groups != null)
                {
                    var results = new List<ReviewContainer>();

                    foreach (var group in obj.response.venue.tips.groups)
                    {
                        foreach (var item in group.items)
                        {
                            results.Add(new ReviewContainer()
                            {
                                RemoteSourceId = item.id,
                                Source = (int)SourceType.FourSquare,
                                Text = item.text,
                                Rating = 0,
                                Time = item.createdAt,
                                SourceUrl = item.canonicalUrl ?? "",
                                Plus = item.agreeCount,
                                Minus = item.disagreeCount,
                                User = new UserContainer()
                                {
                                    Source = (int)SourceType.FourSquare,
                                    UserSourceId = item.user.id,
                                    Name = $"{item.user.firstName} {item.user.lastName}",
                                    ImageUrl = item.user.photo != null ? $"{item.user.photo.prefix}50x50{item.user.photo.suffix}" : string.Empty,
                                    ProfileUrl = $"https://ru.foursquare.com/user/{item.user.id}"
                                }
                            });
                        }
                    }

                    return results;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return null;
        }
    }
}
