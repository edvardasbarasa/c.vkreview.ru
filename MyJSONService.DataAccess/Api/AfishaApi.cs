﻿//
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using AngleSharp.Dom;
using AngleSharp.Parser.Html;
using Crawler2.YandexReqLib;
using MyJSONService.DataAccess.Domian;
using MyJSONService.DataAccess.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace MyJSONService.DataAccess
{
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static class AfishaApi
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static List<ReviewContainer> GetReviews(string placeId)
        {
            // Бюро бургеры - 41798
            // Второй ресторан Александра Раппопорта в Петербурге — на этот раз с кантонской кухней  - "351802"

            try
            {
                var getReviewUrl = $"https://www.afisha.ru/spb/restaurant/{placeId}/rev/block-rendered/?o=date&page=1&with_head=True";

                AfishaRootObject obj = new AfishaRootObject();

                using (WebClient wc = new WebClient() { Encoding = Encoding.UTF8 })
                {
                    var json = wc.DownloadString(getReviewUrl);

                    using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                    {
                        DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
                        obj = (AfishaRootObject)serializer.ReadObject(ms);
                    }
                }

                if (obj != null && obj.results != null)
                {
                    var results = new List<ReviewContainer>();

                    foreach (var review in obj.results)
                    {
                        results.Add(new ReviewContainer()
                        {
                            RemoteSourceId = review.id.ToString(),
                            Source = (int)SourceType.Afisha,
                            Text = review.body,
                            Rating = 0,
                            Time = review.created,
                            SourceUrl = obj.restaurant.url_afisha,
                            Plus = review.is_positive_like ? 1 : 0,
                            Minus = review.is_positive_like ? 0 : 1,
                            User = new UserContainer()
                            {
                                Source = (int)SourceType.Afisha,
                                UserSourceId = review.author.id.ToString(),
                                Name = $"{review.author.firstname} {review.author.lastname}" ,
                                ImageUrl = review.author.photo_url,
                                ProfileUrl = $"https://www.afisha.ru/personalpage/{review.author.id}/"
                            }
                        });

                        if(review.comments_number > 0)
                        {
                            results.AddRange(_getReviewResponces(review.id.ToString(), obj.restaurant.url_afisha));
                        }
                    }

                    return results;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return null;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        private static List<ReviewContainer> _getReviewResponces(string reviewId, string placeUrl)
        {
            var getReviewUrl = $"https://www.afisha.ru/rests/api/reviews/{reviewId}/comments/";

            AfishaResponceRootObject obj = new AfishaResponceRootObject();

            using (WebClient wc = new WebClient() { Encoding = Encoding.UTF8 })
            {
                var json = wc.DownloadString(getReviewUrl);

                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
                    obj = (AfishaResponceRootObject)serializer.ReadObject(ms);
                }
            }

            if (obj != null && obj.comments != null)
            {
                var results = new List<ReviewContainer>();

                foreach (var comment in obj.comments)
                {
                    results.Add(new ReviewContainer()
                    {
                        RemoteSourceId = comment.id.ToString(),
                        Source = (int)SourceType.Afisha,
                        Text = comment.text,
                        Rating = 0,
                        Time = comment.created_on,
                        SourceUrl = placeUrl,
                        Plus = comment.thanks_count,
                        Minus = 0,
                        AnswerToRemoteReviewId = reviewId,
                        User = new UserContainer()
                        {
                            Source = (int)SourceType.Afisha,
                            UserSourceId = comment.author.id.ToString(),
                            Name = $"{comment.author.firstname} {comment.author.lastname}",
                            ImageUrl = comment.author.photo_url,
                            ProfileUrl = $"https://www.afisha.ru/personalpage/{comment.author.id}/"
                        }
                    });
                }

                return results;
            }

            return null;
        }
    }
}
