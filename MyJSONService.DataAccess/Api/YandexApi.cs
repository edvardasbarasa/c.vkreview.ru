﻿using Crawler2.YandexReqLib;
using MyJSONService.DataAccess.Domian;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace MyJSONService.DataAccess
{
    public static class YandexApi
    {
        public const string MarketRequestUrl = "https://api.content.market.yandex.ru/";
        public const string Version = "v1";

        /////////////////////////////////////////////////////////////////////
        private static string _getRawUrl(string shopId)
        {
            var url = $"{MarketRequestUrl}{Version}/shop/{shopId}/opinion.json";

            return url;
        }

        /////////////////////////////////////////////////////////////////////
        public static List<ReviewContainer> GetReviews(string placeId)
        {
            GetYandexShopReviesData result = null;

            try
            {
                string contentType;
                var rawUrl = _getRawUrl(placeId);

                byte[] data = RequestData(rawUrl, out contentType);

                if (data != null && data.Length > 0)
                {
                    using (var reader = new StreamReader(new MemoryStream(data), System.Text.Encoding.UTF8))
                    {
                        var myStr = reader.ReadToEnd();

                        try
                        {
                            result = JsonConvert.DeserializeObject<GetYandexShopReviesData>(myStr);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw ex;
                        }
                    }

                    return null; // todo : fill result 
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }

            return null;
        }

        /////////////////////////////////////////////////////////////////////
        private static WebResponse _sendRequest(string url)
        {
            RequestHelper.StartTrace(url);
            try
            {
                var request = WebRequest.Create(url);
                RequestHelper.AddHeaders(request);

                var response = request.GetResponse();

                RequestHelper.ProcessResponceHeaders(response);
                RequestHelper.FinishTrace(url);

                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't send a request. Error: " + ex.Message);
            }

            RequestHelper.FinishTrace(url);
            return null;
        }

        /////////////////////////////////////////////////////////////////////
        public static Stream _callRequest(string url, out string contentType, out long contentLength)
        {
            try
            {
                WebResponse response = _sendRequest(url);

                if (response != null)
                {
                    contentType = response.ContentType;
                    contentLength = response.ContentLength;

                    return response.GetResponseStream();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't call a request data. Error: " + ex.Message);
            }

            contentType = null;
            contentLength = 0;
            return null;
        }

        /////////////////////////////////////////////////////////////////////
        public static byte[] RequestData(string url, out string contentType)
        {
            byte[] data = null;
            contentType = null;
            try
            {
                long contentLength = 0;
                Stream stream = _callRequest(url, out contentType, out contentLength);
                if (stream != null)
                {
                    if (contentLength != -1)
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            data = reader.ReadBytes((int)contentLength);
                            reader.Close();
                        }
                    }
                    else
                    {
                        byte[] buffer = new byte[1024];
                        using (MemoryStream ms = new MemoryStream())
                        {
                            while (true)
                            {
                                int readLen = stream.Read(buffer, 0, buffer.Length);
                                if (readLen <= 0)
                                {
                                    data = ms.ToArray();
                                    break;
                                }
                                ms.Write(buffer, 0, readLen);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Can't get the data content from the url " + url);
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Can't get data. Error: " + ex.Message);
            }

            return data;
        }
    }
}
