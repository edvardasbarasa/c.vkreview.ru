﻿//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

using Crawler2.YandexReqLib;
using MyJSONService.DataAccess.Domian;
using MyJSONService.DataAccess.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
namespace MyJSONService.DataAccess
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static class GoogleApi
    {
        private static readonly string _key = "AIzaSyAInnCQSCv2YAhexGCnpsKwLAZYiWgb1EM";

       

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static List<ReviewContainer> GetReviews(string placeId)
        {
            // Бюро - 4 бургеры - "ChIJn9scWggxlkYRxdA3HnR4nJ0"
            // example place id = "ChIJ6Zy7sT8bdkgR4O1HRuTvvEk" ;

            var getReviewUrl = $"https://maps.googleapis.com/maps/api/place/details/json?placeid={placeId}&key={_key}";
            GoogleRootObject obj = new GoogleRootObject();

            using (WebClient wc = new WebClient() { Encoding = Encoding.UTF8 })
            {
                var json = wc.DownloadString(getReviewUrl);

                using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
                    obj = (GoogleRootObject)serializer.ReadObject(ms);
                }
            }

            if (obj != null && obj.result != null && obj.result.reviews != null)
            {
                var results = new List<ReviewContainer>();

                foreach (var review in obj.result.reviews)
                {
                    results.Add(new ReviewContainer()
                    {
                        RemoteSourceId = RequestHelper.CreateMD5($"{review.author_url}_{review.time.ToString()}"),
                        Source = (int)SourceType.Google,
                        Text = review.text,
                        Rating = 0,
                        Time = review.time,
                        SourceUrl = obj.result.url,
                        Plus = 0,
                        Minus = 0,
                        User = new UserContainer()
                        {
                            Source = (int)SourceType.Google,
                            UserSourceId = review.author_url,
                            Name = review.author_name,
                            ImageUrl = review.profile_photo_url,
                            ProfileUrl = review.author_url
                        }
                    });
                }

                return results;
            }

            return null;
        }
    }
}
