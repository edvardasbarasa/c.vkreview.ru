﻿//
//
//////////////////////////////////////////////////////////////////

using MySql.Data.MySqlClient;
using System.Collections.Generic;
using MyJSONService.DataAccess.Domian;
using System;

//////////////////////////////////////////////////////////////////
namespace MyJSONService.DataAccess
{
    //////////////////////////////////////////////////////////////////
    public static class SqlConnection
    {
        private static readonly string connString = string.Empty;

        //////////////////////////////////////////////////////////////////
        static SqlConnection()
        {
            var server = "207.154.203.250";
            var database = "vkreview";
            var uid = "root";
            var password = "da24a78de474ea37cf8265a36d";
            connString = "SERVER=" + server + "; PORT = 3306 ;" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
        }

        //////////////////////////////////////////////////////////////////
        private static int _getUserId(MySqlConnection connection, ReviewContainer review, SourceType type)
        {
            using (MySqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = $" SELECT * FROM user WHERE source_id = '{review.User.UserSourceId}' AND source = {(int)type};";
                string responce = Convert.ToString(cmd.ExecuteScalar());

                if (!string.IsNullOrEmpty(responce.Trim()))
                {
                    var id = 0;

                    if(int.TryParse(responce, out id))
                    {
                        return id;
                    }
                }

                return 0;
            }
        }

        //////////////////////////////////////////////////////////////////
        private static void _createUser(MySqlConnection connection, ReviewContainer review, SourceType type)
        {
            using (MySqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = string.Format("INSERT INTO user (source_id, source, name, image_url, profile_url) " +
                    "VALUES (@sourceUserId, @sourceType, @name, @image_url, @profile_url)");

                cmd.Parameters.AddWithValue("@sourceUserId", review.User.UserSourceId);
                cmd.Parameters.AddWithValue("@sourceType", (int)type);
                cmd.Parameters.AddWithValue("@name", review.User.Name);
                cmd.Parameters.AddWithValue("@image_url", review.User.ImageUrl);
                cmd.Parameters.AddWithValue("@profile_url", review.User.ProfileUrl);
                cmd.ExecuteScalar();
            }
        }

        //////////////////////////////////////////////////////////////////
        private static void _createReview(MySqlConnection connection, ReviewContainer review, int userId, SourceType type)
        {
            using (MySqlCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = string.Format("INSERT INTO review (source, author_id, text, rating, timestamp, source_src, plus, minus, remote_review_id, answer_to_remote_review_id) " +
                    "VALUES (@sourceType, @author_id, @text, @rating, @timestamp, @sourceSrc, @plus, @minus, @remoteReviewId, @answerToRemoteReviewId)");

                cmd.Parameters.AddWithValue("@sourceType", (int)SourceType.Google);
                cmd.Parameters.AddWithValue("@author_id", userId);
                cmd.Parameters.AddWithValue("@text", review.Text);
                cmd.Parameters.AddWithValue("@rating", review.Rating);
                cmd.Parameters.AddWithValue("@timestamp", review.Time);
                cmd.Parameters.AddWithValue("@sourceSrc", review.SourceUrl);
                cmd.Parameters.AddWithValue("@plus", review.Plus);
                cmd.Parameters.AddWithValue("@minus", review.Minus);
                cmd.Parameters.AddWithValue("@remoteReviewId", review.RemoteSourceId);
                cmd.Parameters.AddWithValue("@answerToRemoteReviewId", review.AnswerToRemoteReviewId ?? "");

                cmd.ExecuteScalar();
            }

        }

        //////////////////////////////////////////////////////////////////
        public static List<ReviewContainer> SaveReviews(List<ReviewContainer> reviews, SourceType type)
        {
            if(reviews == null)
            {
                return new List<ReviewContainer>();
            }

            try
            {
                using (var connection = new MySqlConnection(connString))
                {
                    connection.Open();

                    foreach (var review in reviews)
                    {
                        var userId = _getUserId(connection, review, type);

                        if(userId == 0)
                        {
                            _createUser(connection, review, type);
                            userId = _getUserId(connection, review, type);
                        }

                        _createReview(connection, review, userId, type);
                    }

                    connection.Close();
                }

                return reviews;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return new List<ReviewContainer>();
        }
    }
}