﻿//
//
/////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/////////////////////////////////////////////////////////////////////////
namespace MyJSONService.DataAccess
{
    /////////////////////////////////////////////////////////////////////////
    public static class GeneralConstants
    {
        public const string MyKey = ""; // get key for yandex market content API - waiting for responce
        public const string MarketApiVersion1 = "1";
    }

    /////////////////////////////////////////////////////////////////////////
    public enum ResponseType
    {
        Xml = 0,
        Json = 1
    }

    /////////////////////////////////////////////////////////////////////////
    public enum SourceType
    {
        Vk = 0,
        Afisha = 1,
        Google = 2,
        FourSquare = 3
    }
}
