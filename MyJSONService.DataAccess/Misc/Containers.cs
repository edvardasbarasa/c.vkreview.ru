﻿//
//
/////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/////////////////////////////////////////////////////////////////////
namespace MyJSONService.DataAccess.Domian
{

    public class GetYandexShopReviesData
    {
        // form data structure 
        public string Data;
    }

    public class DefaultResult<T>
    {
        public T Value;
        public string Error = "";
    }

    public class UserContainer
    {
        public int Id { get; set; }
        public string UserSourceId { get; set; }
        public int Source { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string ProfileUrl { get; set; }
    }

    public class ReviewContainer
    {
        public int Id { get; set; }
        public string RemoteSourceId { get; set; }
        public int Source { get; set; }
        public UserContainer User { get; set; }
        public string Text { get; set; }
        public int Rating { get; set; }
        public int Time { get; set; }
        public string SourceUrl { get; set; }
        public int Plus { get; set; }
        public int Minus { get; set; }
        public string AnswerToRemoteReviewId { get; set; }
    }
}
