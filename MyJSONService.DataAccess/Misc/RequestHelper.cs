﻿//
//
///////////////////////////////////////////////////////////////////////

using MyJSONService.DataAccess;
using System;
using System.Diagnostics;
using System.Net;
using System.Text;

//////////////////////////////////////////////////////////////////////
namespace Crawler2.YandexReqLib
{
    ///////////////////////////////////////////////////////////////////
    public class RequestHelper
    {
        protected const string HeaderAuthorization = "Authorization";

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        /////////////////////////////////////////////////////////////////////
        public static string GetResponseType(ResponseType responseType)
        {
            switch (responseType)
            {
                case ResponseType.Xml:
                    return "xml";
                case ResponseType.Json:
                    return "json";
                default:
                    return String.Empty;
            }
        }

        /////////////////////////////////////////////////////////////////////
        public static void AddHeaders(WebRequest request)
        {
            request.Headers.Add(HeaderAuthorization, GeneralConstants.MyKey);
        }

        /////////////////////////////////////////////////////////////////////
        public static void StartTrace(string message)
        {
            if (message != null)
            {
                Trace.WriteLine(String.Format("Start request ({0}): {1}", DateTime.Now.ToString("hh:mm:ss"), message));
            }
        }

        /////////////////////////////////////////////////////////////////////
        public static void ProcessResponceHeaders(WebResponse response)
        {
            var loggString = "ProcessResponceHeaders method started.";

            foreach (var header in response.Headers)
            {
                loggString += header + " :  " + response.Headers[header.ToString()] + " .";
            }

            Console.WriteLine(loggString);
        }

        /////////////////////////////////////////////////////////////////////
        public static void FinishTrace(string message)
        {
            if (message != null)
            {
                Console.WriteLine(String.Format("End request ({0}): {1}", DateTime.Now.ToString("hh:mm:ss"), message));
            }
        }
    }
}