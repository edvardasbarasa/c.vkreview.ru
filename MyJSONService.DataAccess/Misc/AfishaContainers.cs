﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyJSONService.DataAccess.Misc
{
    public class Author
    {
        public int id { get; set; }
        public string photo_url { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public bool blocked { get; set; }
    }

    public class Comment
    {
        public int id { get; set; }
        public int created_on { get; set; }
        public string created { get; set; }
        public string text { get; set; }
        public int parent { get; set; }
        public object parent_author { get; set; }
        public List<object> comments { get; set; }
        public Author author { get; set; }
        public bool visible { get; set; }
        public int thanks_count { get; set; }
    }

    public class AfishaResponceRootObject
    {
        public List<object> comment_user_likes { get; set; }
        public string review_id { get; set; }
        public int margin { get; set; }
        public List<Comment> comments { get; set; }
        public int owner_id { get; set; }
    }

    public class WorkingHour
    {
        public string comment { get; set; }
        public bool until_last_visitor { get; set; }
        public string time_start { get; set; }
        public string time_end { get; set; }
        public int day { get; set; }
        public bool by_agreement { get; set; }
        public int id { get; set; }
    }

    public class Tag
    {
        public List<object> tags { get; set; }
        public int id { get; set; }
        public string short_name { get; set; }
        public string name { get; set; }
    }

    public class MainTag
    {
        public List<object> tags { get; set; }
        public int id { get; set; }
        public string short_name { get; set; }
        public string name { get; set; }
    }

    public class Restaurant
    {
        public double rating { get; set; }
        public object k_correlation { get; set; }
        public List<WorkingHour> working_hours { get; set; }
        public string address { get; set; }
        public string menu_discount { get; set; }
        public string site { get; set; }
        public int created_on { get; set; }
        public int likes { get; set; }
        public string breakfast { get; set; }
        public int id { get; set; }
        public string transliteration { get; set; }
        public int last_modified_by_id { get; set; }
        public int geo_place { get; set; }
        public string main_photo_best_in_moscow { get; set; }
        public string main_photo { get; set; }
        public int rating_modify { get; set; }
        public int state { get; set; }
        public double latitude { get; set; }
        public int open_date { get; set; }
        public string email { get; set; }
        public int owner_id { get; set; }
        public string description { get; set; }
        public List<Tag> tags { get; set; }
        public string opening_hours_string { get; set; }
        public int dislikes { get; set; }
        public string url_afisha { get; set; }
        public string phone { get; set; }
        public string description_from_owner { get; set; }
        public List<int> geo_place_parents { get; set; }
        public string slug { get; set; }
        public int last_modified_on { get; set; }
        public string name { get; set; }
        public int moderation_state { get; set; }
        public string buisness_lunch { get; set; }
        public string synonyms { get; set; }
        public double longitude { get; set; }
        public string verdict { get; set; }
        public MainTag main_tag { get; set; }
    }

    public class AfishaResult
    {
        public string body { get; set; }
        public bool is_positive_like { get; set; }
        public Author author { get; set; }
        public int created { get; set; }
        public int changed { get; set; }
        public int comments_number { get; set; }
        public int id { get; set; }
        public int thanks_count { get; set; }
    }

    public class AfishaRootObject
    {
        public Restaurant restaurant { get; set; }
        public bool has_more { get; set; }
        public bool jsonify { get; set; }
        public List<object> review_user_thanks { get; set; }
        public List<AfishaResult> results { get; set; }
        public object user_review { get; set; }
        public object user { get; set; }
        public string with_head { get; set; }
        public int page { get; set; }
        public string current_order { get; set; }
    }
}
