﻿using MyJSONService.DataAccess.Domian;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyJSONService.DataAccess
{
    public static class RequestHandler
    {
        /////////////////////////////////////////////////////////////////////////////
        public static List<ReviewContainer> GetReviews(string placeId, string ourDbId, SourceType sourceType)
        {
            try
            {
                List<ReviewContainer> reviews = null;

                switch (sourceType)
                {
                    case SourceType.Google:
                        {
                            reviews = GoogleApi.GetReviews(placeId);
                            break;
                        }
                    case SourceType.FourSquare:
                        {
                            reviews = FourSquareApi.GetReviews(placeId);
                            break;
                        }
                    case SourceType.Afisha:
                        {
                            reviews = AfishaApi.GetReviews(placeId);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }

                return reviews;// _saveReviews(reviews, sourceType);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return new List<ReviewContainer>();
        }

        /////////////////////////////////////////////////////////////////////////////
        private static List<ReviewContainer> _saveReviews(List<ReviewContainer> reviews, SourceType sourceType)
        {
            List<ReviewContainer> result = new List<ReviewContainer>();

            //var reviews = GoogleApi.GetReviews(placeId);
            //var reviews = YandexApi.GetYandexShopReviewsData(placeId);
            //var reviews = AfishaApi.GetReviews(placeId);
            //var reviews = FourSquareApi.GetReviews(placeId);

            if (reviews != null)
            {
                result = SqlConnection.SaveReviews(reviews, sourceType);
            }

            return result;

            //SqlConnection.SaveReviews(new List<ReviewContainer>()
            //{
            //    new ReviewContainer()
            //    {
            //         Source = (int)SourceType.FourSquare,
            //                Text = "Текстовый текст",
            //                Rating = 0,
            //                Time = 15000000,
            //                SourceUrl = "тестовый URL",
            //                Plus = 0,
            //                Minus = 0,
            //                User = new UserContainer()
            //                {
            //                    Source = (int)SourceType.FourSquare,
            //                    UserSourceId = "Тестовый Source User Id ",
            //                    Name = "Тестовый User Name ",
            //                    ImageUrl = "Тестовый Image Url ",
            //                    ProfileUrl = "Тестовый Profile Url ",
            //                }
            //    }
            //}, sourceType);
        }

    }
}
