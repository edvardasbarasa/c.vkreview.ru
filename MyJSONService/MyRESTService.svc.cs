﻿//
//
////////////////////////////////////////////////////////////////////////////////////

using System;
using MyJSONService.DataAccess.Domian;
using System.Collections.Generic;
using MyJSONService.DataAccess;

/////////////////////////////////////////////////////////////////////////////////////
namespace MyJSONService
{
    ////////////////////////////////////////////////////////////////////////////////
    public class MyRESTService : IMyRESTService
    {
        /////////////////////////////////////////////////////////////////////////////
        public DefaultResult<List<ReviewContainer>> GetReviews(string sourceId, string id, int type)
        {
            try
            {
                var result = RequestHandler.GetReviews(sourceId, id, (SourceType)type);

                return new DefaultResult<List<ReviewContainer>>()
                {
                    Error = "",
                    Value = result
                };
            }
            catch (Exception ex)
            {
                return new DefaultResult<List<ReviewContainer>>()
                {
                    Error = ex.Message,
                    Value = null
                };
            }
        }
    }
}